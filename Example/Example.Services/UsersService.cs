﻿using Example.Database;
using Example.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example.Services
{
	public class UsersService
	{
		public User GetUserById(int id)
		{
			using (ExampleEntities context = new ExampleEntities())
			{
				var user = context.users.Where(x => x.id == id).FirstOrDefault();
				return AutoMapper.Mapper.Map<User>(user);
			}
		}
	}
}
