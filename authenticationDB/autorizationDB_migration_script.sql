-- ----------------------------------------------------------------------------
-- MySQL Workbench Migration
-- Migrated Schemata: example
-- Source Schemata: example
-- Created: Wed Nov 28 09:27:14 2018
-- Workbench Version: 6.3.10
-- ----------------------------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------------------------------------------------------
-- Schema example
-- ----------------------------------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `example` ;

-- ----------------------------------------------------------------------------
-- Table example.AspNetRoles
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `example`.`AspNetRoles` (
  `Id` VARCHAR(128) CHARACTER SET 'utf8mb4' NOT NULL,
  `Name` VARCHAR(256) CHARACTER SET 'utf8mb4' NOT NULL,
  PRIMARY KEY (`Id`));

-- ----------------------------------------------------------------------------
-- Table example.AspNetUserClaims
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `example`.`AspNetUserClaims` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `UserId` VARCHAR(128) CHARACTER SET 'utf8mb4' NOT NULL,
  `ClaimType` LONGTEXT CHARACTER SET 'utf8mb4' NULL,
  `ClaimValue` LONGTEXT CHARACTER SET 'utf8mb4' NULL,
  PRIMARY KEY (`Id`),
  CONSTRAINT `FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId`
    FOREIGN KEY (`UserId`)
    REFERENCES `example`.`AspNetUsers` (`Id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION);

-- ----------------------------------------------------------------------------
-- Table example.AspNetUserLogins
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `example`.`AspNetUserLogins` (
  `LoginProvider` VARCHAR(128) CHARACTER SET 'utf8mb4' NOT NULL,
  `ProviderKey` VARCHAR(128) CHARACTER SET 'utf8mb4' NOT NULL,
  `UserId` VARCHAR(128) CHARACTER SET 'utf8mb4' NOT NULL,
  PRIMARY KEY (`LoginProvider`, `ProviderKey`, `UserId`),
  CONSTRAINT `FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId`
    FOREIGN KEY (`UserId`)
    REFERENCES `example`.`AspNetUsers` (`Id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION);

-- ----------------------------------------------------------------------------
-- Table example.AspNetUserRoles
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `example`.`AspNetUserRoles` (
  `UserId` VARCHAR(128) CHARACTER SET 'utf8mb4' NOT NULL,
  `RoleId` VARCHAR(128) CHARACTER SET 'utf8mb4' NOT NULL,
  PRIMARY KEY (`UserId`, `RoleId`),
  CONSTRAINT `FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId`
    FOREIGN KEY (`RoleId`)
    REFERENCES `example`.`AspNetRoles` (`Id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId`
    FOREIGN KEY (`UserId`)
    REFERENCES `example`.`AspNetUsers` (`Id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION);

-- ----------------------------------------------------------------------------
-- Table example.AspNetUsers
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `example`.`AspNetUsers` (
  `Id` VARCHAR(128) CHARACTER SET 'utf8mb4' NOT NULL,
  `Email` VARCHAR(256) CHARACTER SET 'utf8mb4' NULL,
  `EmailConfirmed` TINYINT(1) NOT NULL,
  `PasswordHash` LONGTEXT CHARACTER SET 'utf8mb4' NULL,
  `SecurityStamp` LONGTEXT CHARACTER SET 'utf8mb4' NULL,
  `PhoneNumber` LONGTEXT CHARACTER SET 'utf8mb4' NULL,
  `PhoneNumberConfirmed` TINYINT(1) NOT NULL,
  `TwoFactorEnabled` TINYINT(1) NOT NULL,
  `LockoutEndDateUtc` DATETIME NULL,
  `LockoutEnabled` TINYINT(1) NOT NULL,
  `AccessFailedCount` INT NOT NULL,
  `UserName` VARCHAR(256) CHARACTER SET 'utf8mb4' NOT NULL,
  PRIMARY KEY (`Id`));
SET FOREIGN_KEY_CHECKS = 1;
