﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example.Services.Mappings
{
	public static class AutoMapperConfiguration
	{
		public static void Configure()
		{
			Mapper.Reset();

			Mapper.Initialize(x =>
			{
				x.AddProfile<DtoToEntityMappingProfile>();
				x.AddProfile<EntityToDtoMappingProfile>();
			});
		}
	}
}
