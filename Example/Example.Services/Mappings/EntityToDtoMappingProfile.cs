﻿using Example.Database;
using Example.Services.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example.Services.Mappings
{
	public class EntityToDtoMappingProfile : Profile
	{
		public EntityToDtoMappingProfile()
		{
			CreateMap<user, User>();
		}
	}
}
