﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example.Services.Models
{
	public class User
	{
		public int Id { get; set; }
		public string Forname { get; set; }
		public string Surname { get; set; }
	}
}
